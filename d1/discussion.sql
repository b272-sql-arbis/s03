[SECTION] INSERTING RECORDS

INSERT INTO artists (name) VALUES ("RiverMaya");
INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 234, "OPM", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 253, "OPM", 6);

[SECTION] --READ and SELECT RECORDS/DATA

-- Display the title and genre of all the songs.
SELECT song_name, genre FROM songs;

-- Display the song name of all the OPM songs.
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display the title of all the songs.
SELECT * FROM songs;

-- Display the title and length of the OPM songs that are more than 2 minutes.
SELECT song_name, length FROM songs WHERE length > 200 AND genre = "OPM";

-- Display the title and length of the OPM songs that are more than 2:50 minutes.
SELECT song_name, length FROM songs WHERE length > 250 AND genre = "OPM";

[SECTION] Updating records

UPDATE songs SET length = 259 WHERE song_name = "214";

UPDATE songs SET song_name = "Kundiman" WHERE song_name = "214";

[SECTION] --DELETE record

DELETE FROM songs WHERE genre = "OPM" AND length > 250;

-- Delete all from the table

DELETE * FROM songs;